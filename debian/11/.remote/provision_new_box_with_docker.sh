#!/usr/bin/env bash
set -e
cd "$(dirname $0)"

source _functions.sh

log "Provisioning new box with docker"

install_deps
create_swap
setup_user
install_docker
install_b2_cli
install_ctop

log "Restarting your server now dawg"

reboot