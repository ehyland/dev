#!/usr/bin/env bash
set -e
cd "$(dirname $0)"

SCRIPT_NAME="$(basename $0)"
REMOTE_SCRIPT_NAME=".remote/$SCRIPT_NAME"
REMOTE="$1"

if [ -z "$REMOTE" ]; then
    echo "Usage: $SCRIPT_NAME <remode-address>"
    exit 1
fi

echo "Copying to $REMOTE..."
scp -r ".remote" "root@$REMOTE:~"

echo "Running $REMOTE_SCRIPT_NAME on $REMOTE..."
ssh "root@$REMOTE" "$REMOTE_SCRIPT_NAME"
