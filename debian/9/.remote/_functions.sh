#!/usr/bin/env bash

set -e

NEW_USER="eamon"
PUB_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDbqJZrC35FY4gp0alwUhoc+j2rTwI9YGpHGLOG/3lcW1TWYtZWvho6EwmukAAkHmNG9sM9ELQl96+ffFw3LcTIJthp998ELG7fq+ljaucY8lXiXG8lACL8n8YWZonkBKBS8oa2DDwy2FOcDAUflaSgnoEeKhq91q4RShu43MyY9+vERUwb+iAN5k9OoJ6JE2uAsUlknTYFuS1WqIzIFLc5ZVBgPrQDKOb+Tz1MKboSUrSmaPp/bmExjDOwf3N9qaUyDDQOzELWBwQzb95AMCsxzwJqX4E3uuxIhk+OSaWcyZDHM9UeH8ZFvPJTAdYbKhiiTXP3gauDqmvOY0+15Do9 ehyland90@gmail.com"
DOCKER_COMPOSE_VERSION="1.22.0"
SWAP_SIZE="4G"

log() {
    echo "----------------------------------"
    echo "==> $@"
    echo "----------------------------------"
}

install_deps() {
    log "Installing dependancies"

    apt-get update -qy
    apt-get dist-upgrade -qy
    apt-get update -qy
    apt-get upgrade -qy
    apt-get autoremove -qy

    apt-get install -qy \
        sudo \
        vim \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common \
        gnupg2
}

create_swap() {
    log "Creating swap"

    fallocate -l $SWAP_SIZE /swapfile
    chmod 600 /swapfile
    mkswap /swapfile
    swapon /swapfile
    sysctl vm.swappiness=20
    sysctl vm.vfs_cache_pressure=50
    
    echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab
    echo 'vm.swappiness=20' | tee -a /etc/sysctl.conf
    echo 'vm.vfs_cache_pressure=50' | tee -a /etc/sysctl.conf
}

setup_user() {
    log "Setting up user"

    groupadd -f docker
    useradd \
        -G sudo,docker \
        --shell /bin/bash \
        --create-home \
        --user-group \
        $NEW_USER

    mkdir -p "/home/$NEW_USER/.ssh"
    echo "$PUB_KEY" >> "/home/$NEW_USER/.ssh/authorized_keys"
    chmod 0700 "/home/$NEW_USER/.ssh"
    chmod 0600 "/home/$NEW_USER/.ssh/authorized_keys"
    chown -R "$NEW_USER:$NEW_USER" "/home/$NEW_USER/.ssh"

    # configure sudoers
    echo "$NEW_USER ALL=(ALL) NOPASSWD: ALL" >> "/etc/sudoers.d/$NEW_USER"
    
    # disable root & password based login 
    sed -i -E 's/^#?\s*PasswordAuthentication\s.*/PasswordAuthentication no/g' /etc/ssh/sshd_config
    sed -i -E 's/^#?\s*PermitRootLogin\s.*/PermitRootLogin no/g' /etc/ssh/sshd_config
    service ssh restart
}

install_docker() {
    log "Installing docker"

    # add docker repository
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"

    # install docker
    apt-get update -qy
    apt-get install -qy docker-ce

    # install docker-compose
    curl -fsSL "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" > /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
}