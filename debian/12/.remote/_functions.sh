#!/usr/bin/env bash

set -e

NEW_USER="eamon"
PUB_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDbqJZrC35FY4gp0alwUhoc+j2rTwI9YGpHGLOG/3lcW1TWYtZWvho6EwmukAAkHmNG9sM9ELQl96+ffFw3LcTIJthp998ELG7fq+ljaucY8lXiXG8lACL8n8YWZonkBKBS8oa2DDwy2FOcDAUflaSgnoEeKhq91q4RShu43MyY9+vERUwb+iAN5k9OoJ6JE2uAsUlknTYFuS1WqIzIFLc5ZVBgPrQDKOb+Tz1MKboSUrSmaPp/bmExjDOwf3N9qaUyDDQOzELWBwQzb95AMCsxzwJqX4E3uuxIhk+OSaWcyZDHM9UeH8ZFvPJTAdYbKhiiTXP3gauDqmvOY0+15Do9 ehyland90@gmail.com"
SWAP_SIZE="4G"

log() {
    echo "----------------------------------"
    echo "==> $@"
    echo "----------------------------------"
}

install_deps() {
    log "Installing dependancies"

    apt-get update -qy
    apt-get dist-upgrade -qy
    apt-get update -qy
    apt-get upgrade -qy
    apt-get autoremove -qy

    apt-get install -qy \
        jq \
        yq \
        htop \
        sudo \
        vim \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common \
        gnupg2
}

create_swap() {
    log "Creating swap"
    SWAPFILE_PATH="/my-swapfile"

    if [[ ! -e "$SWAPFILE_PATH" ]]; then
        fallocate -l $SWAP_SIZE $SWAPFILE_PATH || true
        chmod 600 $SWAPFILE_PATH
        mkswap $SWAPFILE_PATH
        swapon $SWAPFILE_PATH
    else
        echo "$SWAPFILE_PATH already exists"
    fi

    sysctl vm.swappiness=20
    sysctl vm.vfs_cache_pressure=50
    
    echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab
    echo 'vm.swappiness=20' | tee -a /etc/sysctl.conf
    echo 'vm.vfs_cache_pressure=50' | tee -a /etc/sysctl.conf
}

setup_user() {
    log "Setting up user"

    groupadd -f docker
    useradd \
        -G sudo,docker \
        --shell /bin/bash \
        --create-home \
        --user-group \
        $NEW_USER

    mkdir -p "/home/$NEW_USER/.ssh"
    echo "$PUB_KEY" >> "/home/$NEW_USER/.ssh/authorized_keys"
    chmod 0700 "/home/$NEW_USER/.ssh"
    chmod 0600 "/home/$NEW_USER/.ssh/authorized_keys"
    chown -R "$NEW_USER:$NEW_USER" "/home/$NEW_USER/.ssh"

    # configure sudoers
    echo "$NEW_USER ALL=(ALL) NOPASSWD: ALL" >> "/etc/sudoers.d/$NEW_USER"
    
    # configure ssh server
    SSHD_INCLUDE_LINE="Include /etc/ssh/sshd_config.d/*.conf"
    if ! grep -q "$SSHD_INCLUDE_LINE" "/etc/ssh/sshd_config"; then
        echo "$SSHD_INCLUDE_LINE" >> "/etc/ssh/sshd_config"
    fi

    SSHD_CONF_FILE="/etc/ssh/sshd_config.d/60-eamon-sh.conf"
    echo "" > "$SSHD_CONF_FILE"
    echo "GatewayPorts yes" >> "$SSHD_CONF_FILE"
    echo "X11Forwarding yes" >> "$SSHD_CONF_FILE"
    echo "PermitRootLogin no" >> "$SSHD_CONF_FILE"
    echo "PasswordAuthentication no" >> "$SSHD_CONF_FILE"
    echo "" >> "$SSHD_CONF_FILE"

    service ssh restart
}

install_docker() {
    log "Installing docker"

    # Add Docker's official GPG key:
    apt-get update
    apt-get install ca-certificates curl gnupg
    install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    chmod a+r /etc/apt/keyrings/docker.gpg

    # Add the repository to Apt sources:
    echo \
    "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
    "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
    tee /etc/apt/sources.list.d/docker.list > /dev/null
    apt-get update

    # Install the packages
    apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
}

install_b2_cli() {
    log "Installing b2 cli"

    curl -fsSL https://github.com/Backblaze/B2_Command_Line_Tool/releases/latest/download/b2-linux -o /usr/local/bin/b2
    chmod +x /usr/local/bin/b2
}

install_ctop() {
    log "Installing ctop"
    curl -fsSL https://github.com/bcicen/ctop/releases/download/v0.7.7/ctop-0.7.7-linux-amd64 -o /usr/local/bin/ctop
    chmod +x /usr/local/bin/ctop
}